<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;
use Auth;
class PertanyaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth')->except(['index', 'show']);
    }

    public function create(){
        return view('post.create');
    }

    public function store(Request $request){

        $pertanyaan = Pertanyaan::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"],
            "user_id" => Auth::id()
        ]);

        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request["judul"];
        // $pertanyaan->isi = $request["isi"];
        // $pertanyaan->save();

        // // $query = DB::table('pertanyaan')->insert([
        // //     "judul" => $request["judul"],
        // //     "isi" => $request["isi"]
        // //     // "tanggal_dibuat" => $request["tanggal_dibuat"],
        // //     // "tanggal_diperbaharui" => $request["tanggal_diperbaharui"]
        // ]);

        return redirect('/pertanyaan')->with('success', 'Post berhasil disimpan');
    }

    public function index() {
        // $posts = DB::table('pertanyaan')->get();
        $posts = Pertanyaan::all();

        return view('post.index', compact('posts'));
    }

    public function show($id) {
        // $post = DB::table('pertanyaan')->where('id', $id)->first();
        $post = Pertanyaan::find($id);

        return view('post.show', compact('post'));
    }

    public function edit($id) {
        // $post = DB::table('pertanyaan')->where('id', $id)->first();
        $post = Pertanyaan::find($id);

        return view('post.edit', compact('post'));
    }

    public function update($id, Request $request){
        // $query = DB::table('pertanyaan')->where('id', $id)->update([
        //     'judul' => $request['judul'],
        //     'isi' => $request['isi']
        // ]);

        $update = Pertanyaan::where('id', $id)->update([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Post berhasil diupdate');
    }

    public function destroy($id){
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();
        Pertanyaan::destroy($id);

        return redirect('/pertanyaan')->with('success', 'Post berhasil dihapus');
    }
}
