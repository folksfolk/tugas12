<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\KomentarPertanyaan;
use Auth;
class KomentarPertanyaanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(){
        return view('KomentarPertanyaan.create');
    }

    public function store(Request $request, $pertanyaan_id){

        $pertanyaan = KomentarPertanyaan::find($pertanyaan_id);

        $komentar = KomentarPertanyaan::create([
            "isi" => $request["isi"],
            "user_id" => Auth::id(),
            "pertanyaan_id" => $request($pertanyaan)
        ]);

        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->judul = $request["judul"];
        // $pertanyaan->isi = $request["isi"];
        // $pertanyaan->save();

        // // $query = DB::table('pertanyaan')->insert([
        // //     "judul" => $request["judul"],
        // //     "isi" => $request["isi"]
        // //     // "tanggal_dibuat" => $request["tanggal_dibuat"],
        // //     // "tanggal_diperbaharui" => $request["tanggal_diperbaharui"]
        // ]);

        return redirect('/komentar-pertanyaan')->with('success', 'Post berhasil disimpan');
    }

    public function index() {
        // $posts = DB::table('pertanyaan')->get();
        $posts = KomentarPertanyaan::all();

        return view('KomentarPertanyaan.index', compact('posts'));
    }

    public function show($id) {
        // $post = DB::table('pertanyaan')->where('id', $id)->first();
        $post = KomentarPertanyaan::find($id);

        return view('KomentarPertanyaan.show', compact('post'));
    }

    public function edit($id) {
        // $post = DB::table('pertanyaan')->where('id', $id)->first();
        $post = KomentarPertanyaan::find($id);

        return view('KomentarPertanyaan.edit', compact('post'));
    }

    public function update($pertanyaan_id, $id, Request $request){

        $pertanyaan = KomentarPertanyaan::find($pertanyaan_id);

        // $query = DB::table('pertanyaan')->where('id', $id)->update([
        //     'judul' => $request['judul'],
        //     'isi' => $request['isi']
        // ]);

        $update = KomentarPertanyaan::where('id', $id)->update([
            "isi" => $request["isi"],
            "user_id" => Auth::id(),
            "pertanyaan_id" => $request($pertanyaan) 
        ]);

        return redirect('/komentar-pertanyaan')->with('success', 'Post berhasil diupdate');
    }

    public function destroy($id){
        // $query = DB::table('pertanyaan')->where('id', $id)->delete();
        KomentarPertanyaan::destroy($id);

        return redirect('/komentar-pertanyaan')->with('success', 'Post berhasil dihapus');
    }
}
