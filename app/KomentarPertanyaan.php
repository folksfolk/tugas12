<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KomentarPertanyaan extends Model
{
    protected $table = "komentarpertanyaan";

    protected $fillable =["isi"];

    protected $guarded = [];

    public function pertanyaan(){
        return $this->belongsTo('App\Pertanyaan', 'id');
    }
}
