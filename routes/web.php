<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('layouts.app');
});

// Route::get('/', 'HomeController@home')->name("welcome");
// Route::get('/form', 'AuthController@register')->name("form");
// Route::post('/home', 'AuthController@welcome')->name("home");

// Route::get('/', function () {
//     return view('homeTemplate');
// });

Route::get('/data-tables', function() {
    return view('tableTemplate');
});

Route::get('/pertanyaan/create', 'PertanyaanController@create');
Route::post('/pertanyaan', 'PertanyaanController@store');
Route::get('/pertanyaan', 'PertanyaanController@index');
Route::get('pertanyaan/{id}', 'PertanyaanController@show');
Route::get('pertanyaan/{id}/edit', 'PertanyaanController@edit');
ROUTE::put('/pertanyaan/{id}', 'PertanyaanController@update');
ROUTE::delete('/pertanyaan/{id}', 'PertanyaanController@destroy');

Route::get('/komentar-pertanyaan/create', 'KomentarPertanyaanController@create');
Route::post('/komentar-pertanyaan', 'KomentarPertanyaanController@store');
Route::get('/komentar-pertanyaan', 'KomentarPertanyaanController@index');
Route::get('komentar-pertanyaan/{id}', 'KomentarPertanyaanController@show');
Route::get('komentar-pertanyaan/{id}/edit', 'KomentarPertanyaanController@edit');
ROUTE::put('/komentar-pertanyaan/{id}', 'KomentarPertanyaanController@update');
ROUTE::delete('/komentar-pertanyaan/{id}', 'KomentarPertanyaanController@destroy');

Route::resource('pertanyaan', 'PertanyaanController');
Route::resource('pertanyaan', 'KomentarPertanyaanController');
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::middleware('dateMiddleware')->group(function(){
    Route::get('/route-1', 'TestController@test');
    Route::get('/route-2', 'TestController@test');
    Route::get('/route-3', 'TestController@test');
});

Route::get('/route-1', 'TestController@admin');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
