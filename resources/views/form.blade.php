<!DOCTYPE html>
<html>
    <head>
        <title> Formulir Pendaftaran </title>
    </head>

    <body>
        <div>
            <h1> Buat Akun Baru </h1>
            <h2> &nbsp; Formulir Pendaftaran </h2>
        </div>
        <br>
            <form action="{{route('home')}}" method="post">
            @csrf
            <table cellspacing="4" cellpadding="4">
                <tr>
                    <td><strong><label for="nama_depan"> Nama Depan: </label></strong></td>
                    <td><input type="text" placeholder="Nama Anda" name="nama_depan" id="nama_depan"></td>
                </tr>

                <tr>
                    <td><strong><label for="nama_belakang"> Nama Belakang: </label></strong></td>
                    <td><input type="text" placeholder="Nama Anda" name="nama_belakang" id="nama_belakang"></td>
                </tr>

                <tr>
                    <td><strong><label for="gender"> Jenis Kelamin: </label></strong></td>
                    <td>
                        <input type="radio" name="gender" value="0"> Laki-Laki 
                        <input type="radio" name="gender" value="1"> Perempuan 
                        <input type="radio" name="gender" value="2"> Lainnya 
                    </td>
                </tr>

                <tr>
                    <td><strong><label for="kewarganegaraan"> Kewarganegaraan: </label></strong></td>
                    <td>
                    <select>
                        <optgroup label="Asia">
                        <option value="Indonesia">Indonesia</option>
                        <option value="Malaysia">Malaysia</option>
                        <option value="Singapura">Singapura</option>
                        <option value="Jepang">Jepang</option>
                        <option value="China">China</option>
                        <option value="Korea Selatan">Korea Selatan</option>
                        <option value="Korea Utara">Korea Utara</option>
                        </optgroup>
        
                        <optgroup label="Amerika">
                            <option value="Amerika"> Amerika </option>
                            <option value="Brazi"> Brazil </option>
                        </optgroup>
                    </select>
                    </td>
                </tr>

                <tr>
                    <td><strong><label for="bahasa"> Bahasa: </label></strong></td>
                    <td>
                        <input type="checkbox" name="bahasa" value="0"> Indonesia 
                        <input type="checkbox" name="bahasa" value="1"> Inggris 
                        <input type="checkbox" name="bahasa" value="2"> Lainnya 
                    </td>
                </tr>

                <tr>
                    <td><strong><label for="bio"> Bio: </label></strong></td>
                    <td><textarea cols="100" rows="10"></textarea></td>
                </tr>

                <tr>
                    <td colspan= "2"><input type="submit" name="daftar" value="Daftar" method="GET"></td>
                </tr>
            </table>
    </body>
</html>