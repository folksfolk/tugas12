@extends('master')

@section('content')
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Edit Post {{$post->id}}</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="/pertanyaan/{{$post->id}}" method="POST">
                        @csrf
                        @method('PUT')
                    <div class="card-body">
                        <div class="form-group">
                            <label for="judul">Judul Pertanyaan</label>
                            <input type="text" class="form-control" id="judul" name="judul" value="{{ old('judul', $post->judul) }}" placeholder="Masukan Judul">
                            @error('judul')
                                <div class="alert alert danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="isi">Isi Pertanyaan</label>
                            <input type="text" class="form-control" id="isi" name="isi" value="{{ old('isi', $post->isi) }}" placeholder="Masukan Isi">
                            @error('isi')
                                <div class="alert alert danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <!-- <div class="form-group">
                            <label for="tanggal_dibuat">Tanggal dibuat</label>
                            <input type="date" class="form-control" id="tanggal_dibuat" name="tanggal_dibuat">
                        </div>
                        <div class="form-group">
                            <label for="tanggal_diperbaharui">Tanggal diubah</label>
                            <input type="date" class="form-control" id="tanggal_diperbaharui" name="tanggal_diperbaharui">
                        </div>
                        <div class="form-group">
                            <label for="profil">Pembuat Pertanyaan</label>
                            <input type="text" class="form-control" id="profil" nama="profil" placeholder="Masukan Nama">
                        </div>
                        <div class="form-group">
                            <label for="jawaban">Jawabannya</label>
                            <input type="text" class="form-control" id="jawaban" nama="jawaban" placeholder="Masukan Jawaban">
                        </div> -->
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    </form>
                </div>
                <!-- /.card -->
@endsection