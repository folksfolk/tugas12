@extends('master')

@section('content')
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Buat Pertanyaan Baru</h3>
                    </div>
                    <!-- /.card-header -->
                    <!-- form start -->
                    <form role="form" action="/pertanyaan" method="POST">
                        @csrf
                    <div class="card-body">
                        <div class="form-group">
                            <label for="judul">Judul Pertanyaan</label>
                            <input type="text" class="form-control" id="judul" name="judul" placeholder="Masukan Judul">
                            @error('judul')
                                <div class="alert alert danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="isi">Isi Pertanyaan</label>
                            <input type="text" class="form-control" id="isi" name="isi" placeholder="Masukan Isi">
                            @error('isi')
                                <div class="alert alert danger">{{ $message }}</div>
                            @enderror
                        </div>
                        <!-- <div class="form-group">
                            <label for="tanggal_dibuat">Tanggal dibuat</label>
                            <input type="date" class="form-control" id="tanggal_dibuat" name="tanggal_dibuat">
                        </div>
                        <div class="form-group">
                            <label for="tanggal_diperbaharui">Tanggal diubah</label>
                            <input type="date" class="form-control" id="tanggal_diperbaharui" name="tanggal_diperbaharui">
                        </div>
                        <div class="form-group">
                            <label for="profil">Pembuat Pertanyaan</label>
                            <input type="text" class="form-control" id="profil" nama="profil" placeholder="Masukan Nama">
                        </div>
                        <div class="form-group">
                            <label for="jawaban">Jawabannya</label>
                            <input type="text" class="form-control" id="jawaban" nama="jawaban" placeholder="Masukan Jawaban">
                        </div> -->
                    </div>
                    <!-- /.card-body -->

                    <div class="card-footer">
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </div>
                    </form>
                </div>
                <!-- /.card -->
@endsection